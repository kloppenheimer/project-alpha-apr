from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from tasks.models import Task


# Create your views here.
@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            category = form.save()
            category.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {
        "show_tasks": tasks,
    }
    return render(request, "tasks/task_list.html", context)
